import { USER_CHANGE, REGISTER, REGISTER_FAILED, REGISTER_SUCCESS, USER_INFO, USER_INFO_SUCCESS, USER_INFO_FAILED } from "../actions/types";

const INITIAL_STATE = {
    userName: '',
    name: '',
    surname: '',
    email: '',
    password: '',
    loading: false,
    nav: false
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case USER_CHANGE:
            return{
                ...state,
                [action.payload.name]: action.payload.value
            }

        case REGISTER: 
            return {
                ...state,
                loading: true
            }
        
        case REGISTER_FAILED:
            return {
                ...state,
                loading: false
            }

        case REGISTER_SUCCESS:
            return {
                ...state,
                loading: false
            }

        case USER_INFO: 
            return {
                ...state,
                loading: true
            }
        
        case USER_INFO_SUCCESS:
            return {
                ...state,
                loading: false,
                nav: true
            }

        case USER_INFO_FAILED: 
            return {
                ...state,
                loading: false
            }

        default:
            return state;
    }
}