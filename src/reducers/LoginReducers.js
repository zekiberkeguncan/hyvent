import {
    LOGIN,
    LOGIN_FAILED,
    LOGIN_SUCCESS,
    EMAIL_CHANGED_LOGIN,
    PASSWORD_CHANGED_LOGIN
} from '../actions/types';

const INITIAL_STATE = {
    email:'',
    password:'',
    loading: '',
    user:{},
    nav: false
}

export default (state = INITIAL_STATE, action, props) => {
    switch (action.type) {
        case EMAIL_CHANGED_LOGIN:
            return{
                ...state,
                email: action.payload
            }
        
        case PASSWORD_CHANGED_LOGIN:
            return{
                ...state,
                password: action.payload
            }
        
        case LOGIN: 
            return{
                ...state,
                user: action.payload,
                loading: true
            }
            

        case LOGIN_SUCCESS: 
            return {
                ...state,
                loading: false,
                nav: true
            }
        
        case LOGIN_FAILED: 
            return {
                ...state,
                loading: false
            }
        
        default:
            return state;
    
        }
    }