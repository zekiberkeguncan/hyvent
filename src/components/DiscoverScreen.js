import React, { Component } from 'react';
import {
    Text,
    View
} from 'react-native';
import Header from './Header';

export default class DiscoverScreen extends Component {
    render() {
        return(
            <View style = {{flex:1}}>
                <Header/>
                <Text>DiscoverScreen</Text>
            </View>
        );
    }
}