import React, { Component } from 'react';
import {
    Text,
    View,
    TextInput
} from 'react-native';
import firebase from 'firebase';
import Header from './Header';

export default class NoteListScreen extends Component {

    render() {
        return(
            <View style = {styles.mainViewStyle}>
                <Header/>
            </View>
        );
    }
}

const styles = {
    mainViewStyle: {
        height: '100%',
        width: '100%',
        backgroundColor: '#f8f8f8'   
    }
}