import React, { Component } from 'react';
import {
    Text,
    View
} from 'react-native';
import Header from './Header';

export default class CommonNoteListScreen extends Component {
    render() {
        return(
            <View style = {{flex:1}}>
                <Header/>
                <Text>CommonNoteListScreen</Text>
            </View>
        );
    }
}