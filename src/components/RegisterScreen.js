import React, {Component} from 'react';
import {
    Text,
    TextInput,
    View,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import { register, userChange } from '../actions';
import LoginButton from './LoginButton';
import hyventLogo from '../images/hyventlogo.png';
import Header from './Header';
import Spinner from './Spinner';

class RegisterScreen extends Component {

    componentWillReceiveProps(nextProps) {
        if(nextProps.nav) {
            this.props.navigation.navigate('MainScreen');
        }
    }

    clickRegister = () => {
        const {name,surname,userName,email,password} = this.props;
        this.props.register(email,password,userName,name,surname);
    }

    renderButton = () => {
        if (this.props.loading == true) {
            return <Spinner size = 'large'/>;        
        }else{
            return <LoginButton onPress={this.clickRegister}>Kayıt Ol</LoginButton>                
        }
    }

    render() {
        return(
            <View style = {styles.mainViewStyle}>
                <View style = {styles.logoViewStyle}>
                    <Image
                        style={styles.logoStyle}
                        source={hyventLogo}
                    />
                </View> 
                <View style = {styles.registerViewStyle}>
                    <TextInput
                        placeholder='kullanıcı adı'
                        style= {styles.registerInputStyle}
                        onChangeText = {(userName) => this.props.userChange({ name: 'userName', value: userName })}
                    />
                    <TextInput
                        placeholder= "isim"
                        style = {styles.registerInputStyle}
                        onChangeText = {name => this.props.userChange({ name: 'name', value: name })}                        
                    />
                    <TextInput
                        placeholder= "soyisim"
                        style = {styles.registerInputStyle}
                        onChangeText = {surname => this.props.userChange({ name: 'surname', value: surname })}                        
                    />
                    <TextInput
                        placeholder= "e-mail"
                        style = {styles.registerInputStyle}
                        onChangeText = {email => this.props.userChange({ name: 'email', value: email })}                        
                    />
                    <TextInput
                        secureTextEntry
                        placeholder= "parola"
                        style = {styles.registerInputStyle}
                        onChangeText = {password => this.props.userChange({ name: 'password', value: password })}                        
                    />
                    {this.renderButton()}
                </View>
            </View>
        );
    }
}

const styles = {
    mainViewStyle: {
        justifyContent:'flex-start',
        alignItems:'center',
        width: '100%',
        height: '100%',
        backgroundColor: '#26d591', 
    },
    logoViewStyle: {
        height: '20%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoStyle: {
        marginTop: '25%',
        height:72.5,
        width:200
    },
    registerViewStyle: {
        justifyContent:'center',
        alignItems:'center',
        height: '80%',
        width: '100%',
    },
    registerInputStyle: {
        width: '75%',
        height: 40,
        backgroundColor:'white',
        borderRadius:6,
        borderColor: 'white',
        borderWidth: 1.2,
        marginBottom: '5%',
        textAlign: 'center'
    }
}

const mapStateToProps = ({RegisterResponse}) => {
    const { name, surname, userName, email, password, loading, nav } = RegisterResponse;
    return { 
        name, 
        surname,
        userName,
        email, 
        password, 
        loading,
        nav
    };
}

export default connect(mapStateToProps, { userChange, register })(RegisterScreen)