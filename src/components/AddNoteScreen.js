import React, { Component } from 'react';
import {
    Text,
    View,
    TextInput,
    ScrollView
} from 'react-native';
import Header from './Header';
import Button from './LoginButton';

export default class AddNoteScreen extends Component {
    render() {
        return(
            <ScrollView style = {styles.mainViewStyle}>
                <Header/>        
                <View style = {styles.inputViewStyle}>
                    <View style = {styles.bigTitleView}>
                        <Text style = {styles.textStyle}>Başlık</Text>                    
                    </View>
                    <TextInput
                        underlineColorAndroid='#26d591'
                        style = {styles.titleInputStyle}
                        //placeholderTextColor= 'gray'
                        //placeholder= 'Başlık'  
                    />
                    <View style = {styles.littleTitleView}>
                        <Text style = {styles.textStyle}>Not</Text>                    
                    </View>
                    <TextInput
                        underlineColorAndroid='#26d591'
                        style = {styles.contentInputStyle}
                        //placeholderTextColor= 'gray'
                        //placeholder= 'Başlık'  
                        multiline= {true}
                        textAlignVertical= 'top'
                    />
                    <View style = {styles.littleTitleView}>
                        <Text style = {styles.textStyle}>Arkadaşlar</Text>                    
                    </View>
                    <TextInput
                        underlineColorAndroid='#26d591'
                        style = {styles.friendsInputStyle}
                        //placeholderTextColor= 'gray'
                        //placeholder= 'Başlık'  
                        multiline= {true}
                        textAlignVertical= 'top'
                    />
                    <Button>Kaydet</Button>
                </View>
            </ScrollView>
        );
    }
}

const styles = {
    littleTitleView: {
        marginTop: 5,        
        borderRadius: 6,
        padding: 5,
        height: 25,
        backgroundColor: '#26d591',
        justifyContent: 'center',
        alignItems: 'center',
    },
    bigTitleView: {
        borderRadius: 6,
        padding: 5,
        height: 25,
        backgroundColor: '#26d591',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStyle:{
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold'
    },
    inputViewStyle: {
        marginTop: 10,
        alignItems: 'center'
    },
    mainViewStyle: {
        height: '100%',
        width: '100%',
        backgroundColor: 'white'   
    },
    titleInputStyle: {
        borderRadius: 6,
        width: 300,
        height: 50,
    },
    contentInputStyle: {
        borderRadius: 6,
        width: 300,
        height: 100,
    },
    friendsInputStyle:{
        borderRadius: 6,
        width: 300,
        height: 60
    }
}