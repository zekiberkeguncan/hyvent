import React from 'react';
import { 
    Text,
    View,
    Image,
    TouchableOpacity
 } from 'react-native';
 import Icon from 'react-native-vector-icons/Feather';
 import HeaderLogo from '../images/HeaderLogo.png';


 const Header = (props) => {
    return(
        <View style = {styles.headerStyle}>
            <Image style = {{marginTop:3, alignSelf: 'center'}} source={HeaderLogo}/>
            <TouchableOpacity style={{position:'absolute', right:10,top:10}}>
                <Text>
                    <Icon name='bell' size={25} color= 'gray'/>                       
                </Text>
            </TouchableOpacity>
        </View>
    );
 }

 const styles = {
     headerStyle: {
         height: 50,
         backgroundColor: 'white',
         borderBottomColor: 'gray',
         borderBottomWidth: 1
     }
 }

 export default Header;