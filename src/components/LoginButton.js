import React from 'react';
import {
    TouchableOpacity,
    Text,
    View
} from 'react-native';

const LoginButton = (props) => {
    return(
        <TouchableOpacity onPress = {props.onPress} style = {styles.buttonStyle}>
            <Text style = {styles.textStyle}>{props.children}</Text>
        </TouchableOpacity>
    );
}

const styles = {
    buttonStyle: {
        width: '75%',
        height: 40,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6
    },
    textStyle: {
        fontWeight:'bold',
        color:'white'
    }
}

export default LoginButton;