export const EMAIL_CHANGED_LOGIN = 'email_changed_login';
export const PASSWORD_CHANGED_LOGIN = 'password_changed_login';
export const LOGIN = 'login';
export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_FAILED = 'login_failed';

export const USER_CHANGE = 'user_change';
export const REGISTER = 'register';
export const REGISTER_FAILED = 'register_failed';
export const REGISTER_SUCCESS = 'register_success';
export const USER_INFO = 'user_information';
export const USER_INFO_FAILED = 'user_info_failed';
export const USER_INFO_SUCCESS = 'user_info_success';