import firebase from 'firebase';
import {
    LOGIN,
    LOGIN_FAILED,
    LOGIN_SUCCESS,
    EMAIL_CHANGED_LOGIN,
    PASSWORD_CHANGED_LOGIN
} from './types'

export const emailChange = (email) => {
    return(dispatch) => {
        dispatch({
            type: EMAIL_CHANGED_LOGIN,
            payload: email
        });
    }
}

export const passwordChange = (password) => {
    return(dispatch) => {
        dispatch({
            type: PASSWORD_CHANGED_LOGIN,
            payload: password
        });
    }
}

const loginSuccess = (dispatch, user) => {
    dispatch({
        type: LOGIN_SUCCESS,
        payload: user   
    });    

};

const loginFailed = (dispatch) => {
    dispatch({
        type: LOGIN_FAILED
    });
    alert('Giriş Başarısız');
};



export const login = (email,password) => {
    return(dispatch) => {
        dispatch({
            type: LOGIN
        });
        if ((email == undefined || password == undefined) || (email == '' || password == '')){
            loginFailed(dispatch);
        }else{
            firebase.auth().signInWithEmailAndPassword(email,password)
                .then((user) => loginSuccess(dispatch, user))
                .catch(() => loginFailed(dispatch));
            }
        }
    }
