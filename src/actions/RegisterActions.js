import firebase from 'firebase';
import { REGISTER, USER_CHANGE, REGISTER_FAILED, REGISTER_SUCCESS, USER_INFO, USER_INFO_FAILED, USER_INFO_SUCCESS} from "./types";

export const userChange = ({name, value}) => {
    return(dispatch) => {
        console.log('userChange');
        dispatch({
            type: USER_CHANGE,
            payload: { name , value}
        });
    };
}

const registerFailed = (dispatch) => {
    dispatch({
        type: REGISTER_FAILED
    });
}

const registerSuccess = (dispatch) => {
    dispatch({
        type: REGISTER_SUCCESS
    });
}

export const register = (email, password, userName, name, surname) => {
    console.log('registerAction');
    return(dispatch) => {
        dispatch({
            type: REGISTER
        });
        if ((email == undefined || password == undefined) || (email == '' || password == '')) {
            console.log(email);
            console.log(password);
            console.log('Syntax Error');
            registerFailed(dispatch);
        }else{
            firebase.auth().createUserWithEmailAndPassword(email, password)
                .then(() => {
                    userInformation({email, password, userName, name, surname}, dispatch);                    
                }
                )
                .catch((err) => {
                    console.log(email);
                    console.log(err);
                    registerFailed(dispatch);
                })
        }
    }
}

const userInformation = ({ userName, name, surname, email, password}, dispatch) => {
    const { currentUser } = firebase.auth();
    console.log('UserInfo');
    console.log('UserInfoReturn')
    dispatch({
        type: USER_INFO
    });
    firebase.database().ref(`/kullanicilar/${currentUser.uid}/bilgiler`)
        .push({ userName, name, surname, email, password })
            .then(() => {
                console.log('UserInfoSucces')
                dispatch({
                    type: USER_INFO_SUCCESS
                });
            })  
            .catch((err) => {
                console.log(err);
                dispatch({
                    type: USER_INFO_FAILED
                });
            });

}